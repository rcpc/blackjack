#pragma once
#include <unordered_map>
#include <string>
#include <fstream>
#include "ErrorCode.h"

class Config
{
public:
	enum Settings
	{
		NONE,
		minPlayers, 
		maxPlayers,
		botHitsBelow
	};

	Config(const std::string& filePath);

private:

	static Config* m_instance;
	std::unordered_map<std::string, Settings> m_strings;
	std::unordered_map<Settings, int> m_settings;
	std::fstream m_file;

	void readConfigs();
	Config::Settings getSettingFromString(std::string& input);
	void initializeStrings();
	void Config::load(const std::string& filePath);

public:

	/* Operations */
	bool Save() const;

	/* Provider */
	int GetSetting(const Settings& settingType);

	/* Singleton */
	static Config* getInstance(const std::string& filePath = "config.ini");

};

