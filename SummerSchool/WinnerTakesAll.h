#pragma once
#include "IWinPolicy.h"

class WinnerTakesAll : public IWinPolicy
{
public:
	double getPrize(int prizePool) override;
};