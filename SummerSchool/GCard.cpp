#pragma once
#include <string>
#include <algorithm>

#include "GCard.h"

GCard::GCard()
{
	// Empty
}

GCard::GCard(Card* card) : card(card)
{	 
	 std::string suit;
	 {
	 if (card->GetSuit() == Suit::CLUBS)
		 suit = "clubs";
	 if (card->GetSuit() == Suit::DIAMONDS)
		 suit = "diamnonds";
	 if (card->GetSuit() == Suit::HEARTS)
		 suit = "hearts";
	 if (card->GetSuit() == Suit::SPADES)
		 suit = "spades";
	 }
	 
	 char rank;
	 {
		 rank = (card->GetRank());
	 }

	 std::string fileName = "..\\Images\\" + suit + " (" + rank + ").gif";
	 
	 //Texture loaded
	 sf::Texture texture;
	 texture.loadFromFile(fileName);

	 sprite.setTexture(texture);
}
