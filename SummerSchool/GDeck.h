#pragma once

#include<string>
#include<vector>

#include "Deck.h"
#include "GCard.h"
class GCard;

class GDeck
{
public:
	GDeck();
	void LoadFromDeck(const Deck& deck);

private:
	std::string filePath; //Default "Resources/Images"

public:
	std::vector<GCard> m_gCards;
};

