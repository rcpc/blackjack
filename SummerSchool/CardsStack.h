#pragma once

class Card;

class CardStack
{
public:
	CardStack();
	CardStack(Card** cards, size_t numCards);

	// ~CardStack();
protected:
	size_t m_numCards;
	Card** m_cards;
};

