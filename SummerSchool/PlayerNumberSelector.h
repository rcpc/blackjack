#pragma once
#include "IStateClass.h"
#include "Button.h"

class PlayerNumberSelector : public IStateClass
{
public:
	PlayerNumberSelector();
	~PlayerNumberSelector() {};

private:
	Button TwoPlayersButton, ThreePlayersButton, FourPlayersButton;

public:
	void update(sf::RenderWindow& window) override;
	void draw(sf::RenderWindow& window) override;
	void handleEvent(sf::RenderWindow& window) override;
};

