#pragma once

#include "Deck.h"

class Card;

class Shoe : Deck
{
public:
	Shoe();
	~Shoe();

	void SetCards(size_t numCards, Card** cards);
	Card* NextCard();

private:
	// size_t m_numCards;
	// Card** m_cards;
	size_t m_nextCardIndex;
};

