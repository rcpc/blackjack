#pragma once

#include <iostream>

#include "Hand.h"
#include "Card.h"

Hand::Hand() : CardStack()
{
	// EMPTY
}

Hand::~Hand() 
{
}

void Hand::AddCard(Card* card)
{
	size_t newNumCards = m_numCards + 1;
	Card** newCards = new Card * [newNumCards];

	for (size_t idx = 0; idx < m_numCards; ++idx)
	{
		newCards[idx] = m_cards[idx];
	}

	delete[] m_cards;
	m_cards = nullptr;

	// Set the new card at last position
	newCards[newNumCards - 1] = card;

	m_cards = newCards;
	m_numCards = newNumCards;
}

size_t Hand::GetValue() const
{
	size_t val = 0;
	size_t numAces = 0;
	for (int idx = 0; idx < m_numCards; ++idx)
	{
		Rank rank = m_cards[idx]->GetRank();
		size_t value = 0;
		if (rank >= '2' && rank <= '9')
		{
			value = rank - '0';
		}
		else if (rank == 'A')
		{
			value = 11;
			++numAces;
		}
		else
		{
			value = 10;
		}

		val += value;
	}

	while (val > 21 && numAces > 0)
	{
		val -= 10;
		--numAces;
	}
	return val;
}

void Hand::Print()
{
	for (int idx = 0; idx < m_numCards; ++idx)
	{
		printf("\t");
		m_cards[idx]->Print();
	}
}
