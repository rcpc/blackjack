#pragma once

#include "CardsStack.h"

class Card;

class Deck : public CardStack
{
public:
	//Default constructor
	Deck();

	Deck(Card**, size_t);
	~Deck();
	
	size_t GetNumberOfCards() const;
	Card** GetCards() const;
};

