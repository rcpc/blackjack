#pragma once 
#include "Enums.h"
#include <string>
#include <iostream>

class Card
{
public:
	Card();
	Card(const Enums::Rank& rank, const Enums::Suit& suit);
	Card(const Card& other);
	Card(Card&& other);
	~Card() = default;

	void print() const;
	Card& operator= (const Card& other);
	Card& operator= (Card&& other);

	Enums::Rank getRank() const;
	Enums::Suit getSuit() const;
	void setRank(const Enums::Rank& rank);
	void setSuit(const Enums::Suit& suit);

private:
	Enums::Rank m_rank;
	Enums::Suit m_suit;
};