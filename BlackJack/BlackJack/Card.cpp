#include "Card.h"

Card::Card()
{
	m_rank = Enums::Rank::None;
	m_suit = Enums::Suit::None;
}

Card::Card(const Enums::Rank& rank, const Enums::Suit& suit)
{
	m_rank = rank;
	m_suit = suit;
}

Card::Card(const Card& other) :
	m_rank(other.getRank()),
	m_suit(other.getSuit())
{}

Card::Card(Card&& other)
{
	*this = std::move(other);
}

void Card::print() const
{
	std::string rank, suit;

	switch (this->m_rank)
	{
	case Enums::Rank::Two:
		rank = "Two";
		break;
	case Enums::Rank::Three:
		rank = "Three";
		break;
	case Enums::Rank::Four:
		rank = "Four";
		break;
	case Enums::Rank::Five:
		rank = "Five";
		break;
	case Enums::Rank::Six:
		rank = "Six";
		break;
	case Enums::Rank::Seven:
		rank = "Seven";
		break;
	case Enums::Rank::Eight:
		rank = "Eight";
		break;
	case Enums::Rank::Nine:
		rank = "Nine";
		break;
	case Enums::Rank::Ten:
		rank = "Ten";
		break;
	case Enums::Rank::Ace:
		rank = "Ace";
		break;
	case Enums::Rank::Jack:
		rank = "Jack";
		break;
	case Enums::Rank::Queen:
		rank = "Queen";
		break;
	case Enums::Rank::King:
		rank = "King";
		break;

	default:
		rank = "Undefined";
	}

	switch (this->m_suit)
	{
	case Enums::Suit::Clubs:
		suit = "Clubs";
		break;
	case Enums::Suit::Diamond:
		suit = "Diamond";
		break;
	case Enums::Suit::Heart:
		suit = "Heart";
		break;
	case Enums::Suit::Spade:
		suit = "Spade";
		break;

	default:
		suit = "Undefined";
	}

	std::cout << rank << " " << suit << std::endl;
}

Card& Card::operator=(const Card& other)
{
	m_rank = other.getRank();
	m_suit = other.getSuit();

	return (*this);
}

Card& Card::operator=(Card&& other)
{
	m_rank = other.getRank();
	m_suit = other.getSuit();

	other.setRank(Enums::Rank::None);
	other.setSuit(Enums::Suit::None);

	return *this;
}

Enums::Rank Card::getRank() const
{
	return m_rank;
}

Enums::Suit Card::getSuit() const
{
	return m_suit;
}

void Card::setRank(const Enums::Rank& rank)
{
	m_rank = rank;
}

void Card::setSuit(const Enums::Suit& suit)
{
	m_suit = suit;
}
